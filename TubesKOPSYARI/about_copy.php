<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- mobile metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="viewport" content="initial-scale=1, maximum-scale=1" />
    <!-- site metas -->
    <title>About</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!-- owl carousel style -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.2.4/assets/owl.carousel.min.css" />
    <!-- bootstrap css -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
    <!-- style css -->
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <!-- Responsive-->
    <link rel="stylesheet" href="css/responsive.css" />
    <!-- fevicon -->
    <link rel="icon" href="images/fevicon.png" type="image/gif" />
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="css/jquery.mCustomScrollbar.min.css" />
    <!-- Tweaks for older IEs-->
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" />
    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Poppins:400,700&display=swap" rel="stylesheet" />
    <!-- owl stylesheets -->
    <link rel="stylesheet" href="css/owl.carousel.min.css" />
    <link rel="stylesheet" href="css/owl.theme.default.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen" />
  </head>
  <body>
    <!--header section start -->
    <div class="header_section">
      <div class="header_bg">
        <div class="container">
          <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <h1 class="text-primary font-weight-bold">KOPSYARI</h1>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                  <a class="nav-link" href="index_copy.php">Home</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="about_copy.php">About</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="services_copy.php">Services</a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="contact_copy.php">Contact Us</a>
                </li>
              </ul>
              <div class="call_section">
                <ul>
                  <div class="donate_bt">
                    <a href="logout.php"><button type="button" class="btn btn-primary text-center">Logout</button></a>
                  </div>
                </ul>
              </div>
            </div>
          </nav>
        </div>
      </div>
    </div>
    <!--header section end -->
    <!-- about section start -->
    <div class="news_section layout_padding">
      <div class="container">
        <h1 class="news_taital">Tentang Koperasi Syariah</h1>

        <div class="news_section_2">
          <div class="row">
            <div class="col-md-6">
              <div class="news_taital_box">
                <h4 class="make_text">Mengatur Keuangan Anda di Koperasi Syariah itu Mudah!</h4>
                <p class="lorem_text">
                  Koperasi syariah atau koperasi jasa keuangan syariah (KJKS) adalah koperasi yang dijalankan yang kegiatan usahanya bergerak dalam bidang pembiayaan, investasi dan simpanan sesuai pola bagi hasil (syariah). Dengan begitu
                  semua koperasi jasa keuangan syariah telah memiliki payung hukum dan diakui dengan catatan memenuhi ketentuan dalam perundang-undangan yang berlaku.
                </p>
              </div>
            </div>
            <div class="col-md-6">
              <img src="images/img-6.png" class="image_6" style="width: 100%" />
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- about section end -->
    <!-- footer section start -->
    <div class="footer_section layout_padding">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 margin_top">
            <div class="call_text">
              <a href="#"><img src="images/call-icon1.png" /><span class="padding_left_15">Hubungi Kami +629876543210</span></a>
            </div>
            <div class="call_text">
              <a href="#"><img src="images/mail-icon1.png" /><span class="padding_left_15">kopsyari@gmail.com</span></a>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="information_main">
              <h4 class="information_text">Information</h4>
              <p class="many_text">Daftarkan diri anda di Koperasi Syariah melalui website ini</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6">
            <div class="information_main">
              <h4 class="information_text">Helpful Links</h4>
              <div class="footer_menu">
                <ul>
                  <li><a href="index_copy.php">Home</a></li>
                  <li><a href="about_copy.php">About</a></li>
                  <li><a href="services_copy.php">Services</a></li>
                  <li><a href="contact_copy.php">Contact Us</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="information_main">
              <div class="footer_logo">
                <h1 class="text-primary font-weight-bold">KOPSYARI</h1>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- footer section end -->
  <!-- copyright section start -->
  <div class="copyright_section">
    <div class="container">
      <p class="copyright_text">© Created by KOPSYARI</p>
    </div>
  </div>
  <!-- copyright section end -->
  <!-- Javascript files-->
  <script src="js/jquery.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>
  <script src="js/jquery-3.0.0.min.js"></script>
  <script src="js/plugin.js"></script>
  <!-- sidebar -->
  <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
  <script src="js/custom.js"></script>
  <!-- javascript -->
  <script src="js/owl.carousel.js"></script>
  <script src="https:cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
</body>
</html>
